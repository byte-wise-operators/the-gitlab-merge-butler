# GitLab Merge Butler

## Intro

This repository provides the source code for an image containing a script that
will attempt to rebase and merge all identified pending merge requests in a
repository.

You may find this useful if you use either of these two merge methods:

- Merge commit with semi-linear history
- Fast-forward merge

Because these merge methods require that you're up to date with the target
branch before merging.

> _These settings can be found in your project configuration :
> https://gitlab.com/byte-wise-operators/the-gitlab-merge-butler/-/settings/merge_requests
> for instance for this one._

---

## Setup

1. You need to create a label `🚅 AUTOMERGE`, we don't provide a way to
   customize this label for now.
2. If you don't already have one, you need to create a
   [personnal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html).
3. We recommend creating a service account (this is basically a user) with a role allowing the account to merge.\
   Depending on the project, a project access token may be enough, but we found it was easier to manage with a service account.
   ([more infos](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html)).
  

## Usage

1. You need to add the label on the merge request you want to identify.\
2. You need to add the gitlab private token and the gitlab project id in the
gitlab-ci.yml.


Here are the arguments you can pass to the script:

Mandatory arguments:
- Token: A token of the service account.
- Project id: The id of the targeted project.

Optional arguments:
- Maximum number of iterations before failing: The merge butler may fail for some reason and we've included a default retry mechanism. In order to avoid infinite loop, we've set a maximum number of iterations. You can override this value by passing a number as a third argument.
- Project base url: The default value is `https://gitlab.com/api/v4`. You can override this value by passing a string as a fourth argument. This is useful if you're using a self-hosted gitlab instance or if the version of the api changes for instance.
- Open discussions strategy: This parameter allows you to choose how to handle merge requests with open discussions. The default value is `resolve`, meaning the open discussions must be resolved. The butler will filter out merge requests with open discussions (unresolved threads). This setting cannot override the project setting "All threads must be resolved" found [here](https://gitlab.com/byte-wise-operators/the-gitlab-merge-butler/-/settings/merge_requests). You can pass any other value here to override this setting.


> To get the id of the
  project you can use a curl command like this one with your personnal access token:
  ```bash
  curl --header "PRIVATE-TOKEN: glpat-xxx" "https://gitlab.com/api/v4/projects?membership=true" >| projects.json
  ```

Imagine an exemple pipeline where we're merging the merge request using semi-linear history then we create a new commit with the new version number, along with a tag.

Here's an example setup of the merge butler using the `gitlab-ci.yml`:

```yml
rebase-and-merge:
  stage: rebase-and-merge
  image: registry.gitlab.com/byte-wise-operators/the-gitlab-merge-butler:latest
  script:
    - deno run --allow-net --allow-read /gitlab-merge-butler/src/index.ts "<gitlab private token>" "<gitlab project id>"
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: '$CI_COMMIT_REF_NAME == "master" && $CI_COMMIT_TITLE =~ /Update application version.*/'
      when: on_success
```

This sets up the butler to run on every update version commit on master that isn't a tag.


## Precautions

We recommend not adding the label to your merge request unless it's finalized and ready.\
We want to make it clear that if you add the label to a merge request too early in the process, your merge request can be rebased and merged any time someone else
merges on master if your merge request became "mergeable" in the meantime (all discussions closed / pipeline did succeed).\
Even if you've never actually clicked the merge button yourself.\
This is the intended behavior, but we wanted to make that point very clear as we've seen members of our team get the surprise of seeing their merge request merged after fixing a test for instance.

## Contributing

We welcome PR and issues if you have any ideas or suggestions.\
We strongly recommend installing [deno](https://github.com/denoland/deno) locally.\
Also, if you're on VS Code, you can install the deno extension. (Reload your IDE
afterwards.)
