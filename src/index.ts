import {
  byCreatedAt,
  isRebasable,
  makeGitlabClient,
  MergeRequest,
} from './gitlab.ts';

const [
  AUTOMERGE_TOKEN,
  PROJECT_ID,
  MAX_ITERATIONS = 10,
  PROJECT_BASE_URL = 'https://gitlab.com/api/v4/projects',
  OPEN_DISCUSSIONS_STRATEGY = 'resolve',
] = Deno.args;

if (!AUTOMERGE_TOKEN || !PROJECT_ID) {
  throw new Error(`You must pass AUTOMERGE_TOKEN and PROJECT_ID to the script.
  - usage : deno run --allow-read --allow-net --inspect-brk src/index.ts <private token> <project id> [max iterations] [project base url] [discussions must be resolved]`);
}

const gitlabClient = makeGitlabClient(
  PROJECT_BASE_URL,
  PROJECT_ID,
  AUTOMERGE_TOKEN,
);

let attemptCount = 0;

main();

async function main(): Promise<void> {
  attemptCount += 1;
  if (attemptCount > MAX_ITERATIONS) {
    console.error(
      `Aborting procedure after reaching ${MAX_ITERATIONS} attempts.`,
    );
  }
  console.log(`Starting automerge procedure, attempt no ${attemptCount}`);
  const mergeRequests = await gitlabClient.getAllMergeRequests();
  if (mergeRequests.length === 0) {
    console.log('Found no merge request to address, aborting.');
    return;
  }
  console.log(
    `Found ${mergeRequests.length} relevant merge request${
      mergeRequests.length > 1 ? 's' : ''
    }`,
  );

  const overrideBlockingDiscussionsResolved = (mergeRequest: MergeRequest) => ({
    ...mergeRequest,
    blocking_discussions_resolved: OPEN_DISCUSSIONS_STRATEGY === 'resolve'
      ? mergeRequest.blocking_discussions_resolved
      : true,
  });

  const rebasableMergeRequests = mergeRequests
    .map(overrideBlockingDiscussionsResolved)
    .filter(isRebasable)
    .sort(byCreatedAt);

  if (rebasableMergeRequests.length === 0) {
    console.log('Found no rebasable merge request to address, aborting.');
    return;
  }
  console.log(
    `Found ${rebasableMergeRequests.length} rebasable merge request${
      rebasableMergeRequests.length > 1 ? 's' : ''
    }`,
  );

  const rebasableMergeRequestsApprovals = await Promise.all(
    rebasableMergeRequests.map(gitlabClient.getMergeRequestApprovals),
  );

  const [oldestApprovedMergeRequest] = rebasableMergeRequestsApprovals.filter(
    ({ approved }) => !!approved,
  );

  const mergeRequestToRebase = rebasableMergeRequests.find(({ iid }) =>
    iid === oldestApprovedMergeRequest.iid
  );

  if (!mergeRequestToRebase) {
    console.log('Found no merge request to rebase, aborting.');
    return;
  }

  console.log(
    `Attempting to rebase merge request '${mergeRequestToRebase.title}'`,
  );

  const rebase = await gitlabClient.rebase(mergeRequestToRebase.iid);
  if (rebase.merge_error) {
    console.error(
      `Error after rebasing merge request "${mergeRequestToRebase.title}" - ${rebase.merge_error}, moving on.`,
    );
    return main();
  }

  console.log(
    `Successfully rebased merge request "${mergeRequestToRebase.title}".`,
  );

  const merge = await gitlabClient.merge(mergeRequestToRebase.iid);

  if ('message' in merge) {
    console.error(
      `Error after merging merge request "${mergeRequestToRebase.title}" - ${merge.message}, moving on.`,
    );
    return main();
  }

  console.log(
    `Successfully set merge request "${merge.title}" to merge when pipeline succeeds.`,
  );
}
