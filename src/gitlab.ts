import { sleep } from './helpers.ts';

// For more details on the type definitions, see:
// https://docs.gitlab.com/ee/api/merge_requests.html
export type MergeRequest = {
  iid: string;
  created_at: string;
  detailed_merge_status: string;
  has_conflicts: string;
  blocking_discussions_resolved: boolean;
  title: string;
};

type MergeRequestApproval = {
  iid: string;
  approved: boolean;
};

type MergeError = {
  message:
    | 'Unauthorized'
    | 'Method Not Allowed'
    | 'SHA does not match HEAD of source branch'
    | 'Branch cannot be merged';
};

type RebaseStatus = {
  latest_build_started_at: string;
  rebase_in_progress: boolean;
  merge_error?: string | null;
};

type UnauthorizedResponse = {
  message: '401 Unauthorized';
};

type Fetcher = (
  endpoint: string,
  overrides?: Record<string, string>,
) => ReturnType<Response['json']>;

export function isRebasable({
  detailed_merge_status,
  has_conflicts,
  blocking_discussions_resolved,
}: MergeRequest) {
  const REBASABLE_STATUSES = ['mergeable', 'ci_must_pass', 'ci_still_running'];
  return (
    REBASABLE_STATUSES.includes(detailed_merge_status) &&
    !has_conflicts &&
    !!blocking_discussions_resolved
  );
}

export function byCreatedAt(mrA: MergeRequest, mrB: MergeRequest) {
  return mrA.created_at.localeCompare(mrB.created_at);
}

export function makeGitlabClient(
  projectBaseUrl: string,
  projectId: string,
  accessToken: string,
) {
  const options = {
    headers: {
      'Content-Type': 'application/json',
      'PRIVATE-TOKEN': accessToken,
    },
    method: 'GET',
  };
  const repositoryUrl = `${projectBaseUrl}/${projectId}`;

  const fetcher: Fetcher = async (endpoint: string, overrides = {}) => {
    const response = await fetch(repositoryUrl + endpoint, {
      ...options,
      ...overrides,
    });
    return response.json();
  };

  return {
    getAllMergeRequests: getAllMergeRequests(fetcher),
    getUpToDateStatus: getUpToDateStatus(fetcher),
    getMergeRequestApprovals: getMergeRequestApprovals(fetcher),
    waitForMergeRequestRebase: waitForMergeRequestRebase(fetcher),
    rebase: rebase(fetcher),
    merge: merge(fetcher),
  };
}

function isRequestAuthorized(
  data: MergeRequest[] | UnauthorizedResponse,
): data is MergeRequest[] {
  // this is a quick hack because GitLab's API
  // doesn't return an array on unauthorized request
  return Array.isArray(data);
}

function getAllMergeRequests(fetcher: Fetcher) {
  return async function () {
    const label = encodeURI('🚅 AUTOMERGE');
    const defaultBranch = await getDefaultBranch(fetcher);
    const endpoint =
      `/merge_requests?labels=${label}&state=opened&wip=no&target_branch=${defaultBranch}`;
    const data = await fetcher(endpoint);
    if (!isRequestAuthorized(data)) {
      throw new Error(
        `Error while fetching the merge requests\nExpected an array but got: ${
          JSON.stringify(
            data,
          )
        }`,
      );
    }

    const mergeRequestsWithUpToDateStatus = await Promise.all(
      data.map<Promise<MergeRequest>>(
        getUpToDateStatus(fetcher),
      ),
    );

    return mergeRequestsWithUpToDateStatus;
  };
}

async function getDefaultBranch(fetcher: Fetcher) {
  const { default_branch } = await fetcher('/');
  return default_branch;
}

function getUpToDateStatus(fetcher: Fetcher) {
  return async function ({ iid }: Partial<MergeRequest>) {
    const endpoint = `/merge_requests/${iid}?with_merge_status_recheck=true`;
    let data: MergeRequest, detailed_merge_status: string;
    const PENDING_STATUS = ['unchecked', 'checking'];
    do {
      data = await fetcher(endpoint);
      ({ detailed_merge_status } = data);
    } while (PENDING_STATUS.includes(detailed_merge_status));
    return data;
  };
}

function getMergeRequestApprovals(fetcher: Fetcher) {
  return async function ({ iid }: MergeRequest) {
    const endpoint = `/merge_requests/${iid}/approvals`;
    return fetcher(endpoint) as Promise<MergeRequestApproval>;
  };
}

function getMergeRequestRebaseStatus(fetcher: Fetcher) {
  return async function ({ iid }: Partial<MergeRequest>) {
    const endpoint = `/merge_requests/${iid}?include_rebase_in_progress=true`;
    return fetcher(endpoint) as Promise<RebaseStatus>;
  };
}

function rebase(fetcher: Fetcher) {
  return async function (iid: string) {
    const endpoint = `/merge_requests/${iid}/rebase`;
    await fetcher(endpoint, { method: 'PUT' });
    return waitForMergeRequestRebase(fetcher)(iid);
  };
}

function waitForMergeRequestRebase(fetcher: Fetcher) {
  return async function (iid: string) {
    const MAX_ITERATIONS = 10;
    let iteration = 0;
    const timestamp = Date.now();
    let rebaseStatus: RebaseStatus;

    do {
      rebaseStatus = await getMergeRequestRebaseStatus(fetcher)({ iid });
      // If the rebase is already in progress, move on
      if (rebaseStatus.rebase_in_progress) break;
      // If the latest build occurred after the timestamp
      // It means the rebase already started but already finished
      const latest_build = new Date(rebaseStatus.latest_build_started_at);
      if (latest_build.valueOf() > timestamp) break;
      iteration += 1;
      console.log(`Waiting for rebase to start, iteration: ${iteration} `);
      await sleep(1000);
    } while (!rebaseStatus.rebase_in_progress && iteration < MAX_ITERATIONS);

    while (rebaseStatus.rebase_in_progress) {
      await sleep(1000);
      rebaseStatus = await getMergeRequestRebaseStatus(fetcher)({ iid });
    }
    return rebaseStatus;
  };
}

function merge(fetcher: Fetcher) {
  return async function (iid: string): Promise<MergeRequest | MergeError> {
    // This forces merge status to be recalculated
    // It could be required after a rebase
    await getUpToDateStatus(fetcher)({ iid });
    const endpoint = `/merge_requests/${iid}/merge`;
    const body = JSON.stringify({ merge_when_pipeline_succeeds: true });
    return fetcher(endpoint, { method: 'PUT', body });
  };
}
