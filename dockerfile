FROM denoland/deno:1.10.3

WORKDIR /gitlab-merge-butler

# Prefer not to run as root.
USER deno

# These steps will be re-run upon each file change in your working directory:
COPY . .
